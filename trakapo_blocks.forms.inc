<?php

/**
 * Form builder for the block type add form
 */
function trakapo_blocks_type_add_form($form, &$form_state, $type = NULL) {

  $form['type'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine Name'),
    '#size' => 60,
    '#maxlength' => 32,
    '#required' => TRUE,
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Block Type Name'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  if (!empty($type)) {
    $query = db_select('trakapo_block_type', 't')
      ->fields('t')
      ->condition('t.type', $type)
      ->execute()
      ->fetchAssoc();

    if (empty($query)) {
      drupal_goto('admin/content/trakapo-blocks-list');
    }

    $form['type']['#default_value'] = $query['type'];
    $form['type']['#disabled'] = TRUE;
    $form['title']['#default_value'] = $query['title'];
    $form_state['type'] = $type;
  }

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Block Type'),
  );

  return $form;
}

/**
 * Validation handler for block type add form
 */
function trakapo_blocks_type_add_form_validate($form, &$form_state) {
  if (empty($form_state['type'])) {
    $query = db_select('trakapo_block_type', 't')
          ->fields('t')
          ->condition('t.type', $form_state['values']['type'])
          ->execute()
          ->fetchAssoc();

    if (!empty($query)) {
      form_set_error('type', 'Machine name must be unique');
    }
  }
}

/**
 * Submit handler for block type add form
 */
function trakapo_blocks_type_add_form_submit($form, &$form_state) {
  if (empty($form_state['type'])) {
    $query = db_insert('trakapo_block_type')
            ->fields(array(
              'type' => $form_state['values']['type'],
              'title' => $form_state['values']['title']
            ))
            ->execute();
  } else {
    $query = db_update('trakapo_block_type')
        ->fields(array(
          'title' => $form_state['values']['title']
        ))
        ->condition('type', $form_state['type'])
        ->execute();
  }

  entity_info_cache_clear();
  menu_rebuild();

  $form_state['redirect'] = 'admin/structure/trakapo-blocks';
}

/**
 * Form builder for the general form
 */
function trakapo_blocks_block_form($form, &$form_state, $block = NULL) {
  $save_string = t('Save Block');

  $form['admin_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Administration Title'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => TRUE,
  );

  // If no block, make a default up
  if (is_string($block)) {
    $block = trakapo_blocks_block_new(array('type' => $block));
    $save_string = t('Add Block');
  } else {
    $form['admin_title']['#default_value'] = $block->admin_title;
    $form['status']['#default_value'] = $block->status;
  }

  // Attach the fields
  field_attach_form('trakapo_block', $block, $form, $form_state);

  $form_state['block'] = $block;

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => $save_string,
  );

  return $form;
}

/**
 * Submit handler for the block creation form
 */
function trakapo_blocks_block_form_validate($form, &$form_state) {
  $block = $form_state['block'];
  field_attach_validate('trakapo_block', $block);
}

/**
 * Submit handler for the block creation form
 */
function trakapo_blocks_block_form_submit($form, &$form_state) {
  $block = $form_state['block'];
  entity_form_submit_build_entity('trakapo_block', $block, $form, $form_state);

  trakapo_blocks_block_save($block);

  $form_state['redirect'] = 'admin/content/trakapo-blocks-list';
  drupal_set_message('Block saved');
}

/**
 * Form builder for the block delete form
 */
function trakapo_blocks_delete_form($form, &$form_state, $block) {
  $form_state['block'] = $block;

  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'trakapo_blocks') . '/trakapo_blocks.forms.inc';

  $form['#submit'][] = 'trakapo_blocks_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete the block?'),
    '',
    '<p>' . t('Deleting this block cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit handler for the blocks creation form
 */
function trakapo_blocks_delete_form_submit($form, &$form_state) {
  $block = $form_state['block'];

  trakapo_blocks_block_delete($block->block_id);

  $form_state['redirect'] = 'admin/content/trakapo-blocks-list';
  drupal_set_message(t('Block has been deleted.'));
}

/**
 * Form builder for the block type add form
 */
function trakapo_blocks_target_add_form($form, &$form_state, $id = NULL) {

  // Title field
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('target Title'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  // Get all of the existing block types to use in form
  $query = db_select('trakapo_block_type', 't')
      ->fields('t', array('type', 'title'))
      ->execute()
      ->fetchAll();

  $types = array();

  // Build arrays for the block type options and later entitiyfieldquery
  foreach ($query as $result) {
    $types[$result->type] = $result->title;
  }

  // Block types selection
  $form['block_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Block Types Available'),
    '#options' => $types,
    '#required' => TRUE,
  );

  // If we're editing an existing target
  if (!empty($id)) {
    $query = db_select('trakapo_target', 't')
      ->fields('t')
      ->condition('t.id', $id)
      ->execute()
      ->fetchAssoc();

    // Check it's an actual target
    if (empty($query)) {
      drupal_goto('admin/structure/trakapo-targets');
    }

    // Set defaults
    $data = unserialize($query['data']);

    $form['title']['#default_value'] = $query['title'];
    $form['block_types']['#default_value'] = $data['block_types'];
    $form_state['target_id'] = $id;
  }

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Block Type'),
  );

  return $form;
}

/**
 * Submit handler for block type add form
 */
function trakapo_blocks_target_add_form_submit($form, &$form_state) {

  if (empty($form_state['target_id'])) {
    $data = array(
      'block_types' => array_filter($form_state['values']['block_types']),
    );

    sort($data['block_types']);

    $target_id = trakapo_blocks_generate_uuid();
    $form_state['target_id'] = $target_id;

    $query = db_insert('trakapo_target')
            ->fields(array(
              'id' => $target_id,
              'title' => $form_state['values']['title'],
              'data' => serialize($data),
            ))
            ->execute();
  } else {
    $query = db_select('trakapo_target', 't')
      ->fields('t')
      ->condition('t.id', $form_state['target_id'])
      ->execute()
      ->fetchAssoc();

    // Set defaults
    $data = unserialize($query['data']);
    $data['block_types'] = array_filter($form_state['values']['block_types']);
    sort($data['block_types']);

    $query = db_update('trakapo_target')
            ->fields(array(
              'title' => $form_state['values']['title'],
              'data' => serialize($data),
            ))
            ->condition('id', $form_state['target_id'])
            ->execute();
  }

  $data = array(
    'selector' => '.trakapo-target-' . $form_state['target_id'],
    'types' => $data['block_types'],
    'source' => 'drupal',
    'title' => $form_state['values']['title'],
  );

  $queue_item = array(
    'request_type' => 'update',
    'fact_type' => 'ir_target',
    'id' => $form_state['target_id'],
    'data' => $data,
  );

  $queue = DrupalQueue::get('trakapo_blocks');
  $queue->createItem($queue_item);

  $form_state['redirect'] = 'admin/content/trakapo-targets';
}

/**
 * Helper function to generate an ID for our entities
 */
function trakapo_blocks_generate_uuid() {
  // The field names refer to RFC 4122 section 4.1.2.
  return sprintf('%04x%04x%04x4%03x%04x%04x%04x%04x',
    // 32 bits for "time_low".
    mt_rand(0, 65535), mt_rand(0, 65535),
    // 16 bits for "time_mid".
    mt_rand(0, 65535),
    // 12 bits after the 0100 of (version) 4 for "time_hi_and_version".
    mt_rand(0, 4095),
    bindec(substr_replace(sprintf('%016b', mt_rand(0, 65535)), '10', 0, 2)),
    // 8 bits, the last two of which (positions 6 and 7) are 01, for "clk_seq_hi_res"
    // (hence, the 2nd hex digit after the 3rd hyphen can only be 1, 5, 9 or d)
    // 8 bits for "clk_seq_low" 48 bits for "node".
    mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535)
  );
}

/**
 * Form builder for the target delete form
 */
function trakapo_blocks_target_delete_form($form, &$form_state, $id) {
  $form_state['target_id'] = $id;

  // Ensure this include file is loaded when the form is rebuilt from the cache.
  $form_state['build_info']['files']['form'] = drupal_get_path('module', 'trakapo_blocks') . '/trakapo_blocks.forms.inc';

  $form['#submit'][] = 'trakapo_blocks_target_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete the target?'),
    '',
    '<p>' . t('Deleting this target cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit handler for the target delete form
 */
function trakapo_blocks_target_delete_form_submit($form, &$form_state) {
  $query = db_delete('trakapo_target')
          ->condition('id', $form_state['target_id'])
          ->execute();

  $queue_item = array(
    'request_type' => 'remove',
    'fact_type' => 'ir_target',
    'id' => $form_state['target_id'],
  );

  $queue = DrupalQueue::get('trakapo_blocks');
  $queue->createItem($queue_item);

  $form_state['redirect'] = 'admin/content/trakapo-targets';
  drupal_set_message(t('target has been deleted.'));
}

/**
 * Inline form for setting default block within target
 */
function trakapo_blocks_target_default_form($form, &$form_state, $id, $block_types) {
  $options = _trakapo_blocks_get_default_options($block_types);

  $query = db_select('trakapo_target', 't')
        ->fields('t', array('data'))
        ->condition('id', $id)
        ->execute()
        ->fetchAssoc();

  // Default block selection
  $form['default_block'] = array(
    '#type' => 'select',
    '#title' => t('Default Block'),
    '#options' => $options,
    '#required' => TRUE,
  );

  // Check if it's already got a default block value set
  if (!empty($query)) {
    $data = unserialize($query['data']);
    // If we've got a default value, set it
    if (!empty($data['default_block'])) {
      $form['default_block']['#default_value'] = $data['default_block'];
    }
  }

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Default Block'),
  );

  $form_state['target_id'] = $id;

  return $form;
}

/**
 * Submit handler for target default form
 */
function trakapo_blocks_target_default_form_submit($form, &$form_state) {
  // Get the data for the update
  $query = db_select('trakapo_target', 't')
          ->fields('t', array('data'))
          ->condition('id', $form_state['target_id'])
          ->execute()
          ->fetchAssoc();

  // Unserialise and set values
  $data = unserialize($query['data']);
  $data['default_block'] = $form_state['values']['default_block'];

  // Update
  $update = db_update('trakapo_target')
          ->fields(array(
            'data' => serialize($data),
          ))
          ->condition('id', $form_state['target_id'])
          ->execute();
}

/**
 * Helper function to get block types and give allowed values
 */
function _trakapo_blocks_get_default_options($block_types) {
  $blocks = &drupal_static(__FUNCTION__);
  if (!isset($blocks)) {
    $blocks = array();

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'trakapo_block');
    $result = $query->execute();

    if (isset($result['trakapo_block'])) {
      foreach ($result['trakapo_block'] as $block) {
        $loaded_block = trakapo_blocks_load($block->block_id);

        if (empty($blocks[$block->type])) {
          $blocks[$block->type] = array(
            $block->block_id => $loaded_block->admin_title,
          );
        } else {
          $blocks[$block->type][$block->block_id] = $loaded_block->admin_title;
        }
      }
    }
  }

  $output = array();

  foreach ($block_types as $block_type) {
    foreach ($blocks[$block_type] as $key => $value) {
      $output[$key] = $value;
    }
  }

  return array_filter($output);
}
