<?php

/**
 * Implements hook_views_data()
 */
function trakapo_blocks_views_data() {
  $data = array();

  $data['trakapo_block']['table']['group']  = t('Trakapo Block');

  $data['trakapo_block']['table']['base'] = array(
    'field' => 'block_id',
    'title' => t('Trakapo Block'),
    'help' => t('A Trakapo Block'),
  );
  $data['trakapo_block']['table']['entity type'] = 'trakapo_block';

  $data['trakapo_block']['table']['default_relationship'] = array(
    'trakapo_block_revision' => array(
      'table' => 'trakapo_block_revision',
      'field' => 'revision_id',
    ),
  );

  // Expose the Block ID.
  $data['trakapo_block']['block_id'] = array(
    'title' => t('Block ID'),
    'help' => t('The unique internal identifier of the block.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'numeric' => TRUE,
    ),
  );

  // Expose the Block admin title
  $data['trakapo_block']['admin_title'] = array(
    'title' => t('Admin Title'),
    'help' => t('The admin title of the block.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Expose the Block admin title
  $data['trakapo_block']['type'] = array(
    'title' => t('Block Type'),
    'help' => t('The type of block.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Expose the Block status
  $data['trakapo_block']['status'] = array(
    'title' => t('Status'),
    'help' => t('The status of the block.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['trakapo_block']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all the available operations links for the block.'),
      'handler' => 'trakapo_blocks_handler_field_block_operations',
    ),
  );

  return $data;
}
