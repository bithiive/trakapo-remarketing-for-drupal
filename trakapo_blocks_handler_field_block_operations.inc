<?php

/**
 * Field handler to present an order's operations links.
 */
class trakapo_blocks_handler_field_block_operations extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['block_id'] = 'block_id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $block_id = $this->get_value($values, 'block_id');

    // Get the operations links.
    $links = menu_contextual_links('trakapo_block', 'admin/content/trakapo-blocks', array($block_id));

    if (!empty($links)) {
      return theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline', 'operations'))));
    }
  }
}
