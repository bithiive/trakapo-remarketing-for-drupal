<?php

class TrakapoBlocksEntityController extends EntityAPIController {
  public function create(array $values = array()) {
    $values += array(
      'type' => $values['type'],
      'uid' => $GLOBALS['user']->uid,
      'status' => 1,
      'created' => 0,
      'changed' => 0,
      'is_new' => TRUE,
      'revision_id' => NULL,
      'block_id' => NULL,
    );

    return parent::create($values);
  }

  public function save($block, DatabaseTransaction $transaction = NULL) {
    global $user;

    // Hardcode the changed time.
    $block->changed = REQUEST_TIME;

    if (empty($block->{$this->idKey}) || !empty($block->is_new)) {
      // Set the creation timestamp if not set, for new entities.
      if (empty($block->created)) {
        $block->created = REQUEST_TIME;
      }
    }

    // Setup revision info
    $block->revision_timestamp = REQUEST_TIME;
    $block->revision_uid = $user->uid;

    // Determine if we will be inserting a new block.
    $block->is_new = empty($block->block_id);

    return parent::save($block, $transaction);
  }
}
