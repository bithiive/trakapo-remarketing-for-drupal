<div class="trakapo-target-<?php echo $target_id; ?>">
  <?php if (!empty($block)): ?>
    <?php echo render($block); ?>
  <?php endif; ?>
</div>
